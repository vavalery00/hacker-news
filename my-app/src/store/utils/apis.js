import axios from "axios";
const BASE_API_URL = process.env.REACT_APP_BASE_API_URL;

export const getStory = async (id) => {
  try {
    const { data: story } = await axios.get(`${BASE_API_URL}/item/${id}.json`);
    return story;
  } catch (error) {
    console.error("Error while getting a story.");
  }
};

function removeDead(posts) {
  return posts.filter(Boolean).filter(({ dead }) => dead !== true);
}

function removeDeleted(posts) {
  return posts.filter(({ deleted }) => deleted !== true);
}

export const getComment = async (id) => {
  try {
    const { data: comment } = await axios.get(
      `${BASE_API_URL}/item/${id}.json`
    );
    return comment;
  } catch (error) {
    console.error("Error while getting a comment.");
  }
};

export const getComments = async (commentsIds) => {
  if (!commentsIds) return [];
  const comments = await Promise.all(commentsIds.map((id) => getComment(id)));
  return removeDead(removeDeleted(comments));
};

const getTotalCount = async (id) => {
  let sum = 1;
  const comment = await getComment(id);
  const { kids: replies } = comment;
  if (comment.dead || comment.deleted) return 0;
  if (replies) {
    const nextLevel = await Promise.all(
      replies.map((elId) => getTotalCount(elId))
    );
    nextLevel.forEach((el) => {
      sum = sum + el;
    });
    return sum;
  }
  return sum;
};

export const getStoryCommentsTotalCount = async (storyId) => {
  let sum = 0;
  const { kids: rootComments } = await getStory(storyId);
  if (rootComments) {
    const rootComentsTotalCounts = await Promise.all(
      rootComments.map((elId) => getTotalCount(elId))
    );
    rootComentsTotalCounts.forEach((number) => {
      sum = sum + number;
    });
  }
  return sum;
};

export const getStories = async (_, { rejectWithValue }) => {
  try {
    const response = await axios.get(`${BASE_API_URL}/newstories.json`);
    const { data: storyIds } = response;
    const stories = await Promise.all(storyIds.slice(0, 150).map(getStory));

    if (response.status !== 200) {
      console.error("Error while getting list of stories.");
    }
    return stories;
  } catch (error) {
    return rejectWithValue(error.message);
  }
};
