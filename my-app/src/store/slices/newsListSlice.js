import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getStories } from "../utils/apis";

export const fetchStories = createAsyncThunk(
  "newsList/fetchStories",
  getStories
);

const initialState = {
  news: [],
  status: null,
  error: null,
};

export const newsListSlice = createSlice({
  name: "newsList",
  initialState,
  reducers: {
    addNews: (state, action) => {
      state.news = action.payload;
    },
  },
  extraReducers: {
    [fetchStories.pending]: (state, action) => {
      state.status = "loading";
      state.error = null;
    },
    [fetchStories.fulfilled]: (state, action) => {
      state.status = "resolved";
      state.news = action.payload;
    },
    [fetchStories.rejected]: (state, action) => {
      state.status = "rejected";
      state.error = action.payload;
    },
  },
});

export const { addNews } = newsListSlice.actions;

export default newsListSlice.reducer;
