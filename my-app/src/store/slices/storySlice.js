import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  getComments,
  getStory,
  getStoryCommentsTotalCount,
} from "../utils/apis";

export const fetchStory = createAsyncThunk(
  "story/fetchStory",
  async (id, thunkAPI) => {
    const story = await getStory(id);
    const comments = await getComments(story.kids);
    await thunkAPI.dispatch(fetchTotalCount(id));
    return { story, comments };
  }
);

export const fetchComments = createAsyncThunk(
  "story/fetchComments",
  async (id, thunkAPI) => {
    const story = await getStory(id);
    await thunkAPI.dispatch(fetchTotalCount(id));
    return await getComments(story.kids);
  }
);

export const fetchTotalCount = createAsyncThunk(
  "story/fetchTotalCount",
  getStoryCommentsTotalCount
);

const initialState = {
  story: {},
  rootComments: [],
  commentsTotalCount: 0,
  storyLoadingStatus: null,
  storyLoadingError: null,
  commentsLoadingStatus: null,
  commentsLoadingError: null,
  commentsTotalCountStatus: null,
  commentsTotalCountError: null,
};

export const storySlice = createSlice({
  name: "story",
  initialState,
  reducers: {
    updateComments: (state, action) => {
      state.rootComments = action.payload;
    },
  },
  extraReducers: {
    [fetchStory.pending]: (state) => {
      state.storyLoadingStatus = "loading";
      state.commentsLoadingError = null;
    },
    [fetchStory.fulfilled]: (state, action) => {
      state.storyLoadingStatus = "resolved";
      state.story = action.payload.story;
      state.rootComments = action.payload.comments;
    },
    [fetchStory.rejected]: (state, action) => {},
    [fetchComments.pending]: (state) => {
      state.commentsLoadingStatus = "loading";
      state.commentsLoadingError = null;
    },
    [fetchComments.fulfilled]: (state, action) => {
      state.commentsLoadingStatus = "resolved";
      state.rootComments = action.payload;
    },
    [fetchComments.rejected]: (state, action) => {},
    [fetchTotalCount.pending]: (state) => {
      state.commentsTotalCountStatus = "loading";
      state.commentsTotalCountError = null;
    },
    [fetchTotalCount.fulfilled]: (state, action) => {
      state.commentsTotalCountStatus = "resolved";
      state.commentsTotalCount = action.payload;
    },
    [fetchTotalCount.rejected]: (state, action) => {},
  },
});

export const { addStory } = storySlice.actions;

export default storySlice.reducer;
