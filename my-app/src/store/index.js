import { configureStore } from "@reduxjs/toolkit";
import newsListReducer from "./slices/newsListSlice";
import storyReducer from "./slices/storySlice";

export const store = configureStore({
  reducer: {
    news: newsListReducer,
    story: storyReducer,
  },
});
