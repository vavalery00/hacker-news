import { Routes, Route } from "react-router-dom";
import "./App.css";

import { NotFoundPage } from "./pages/NotFoundPage";
import { StoryPage } from "./pages/StoryPage";
import { HomePage } from "./pages/HomePage";
import { Layout } from "./components/Layout";
import { Provider } from "react-redux";

import { store } from "./store/index";

function App() {
  return (
    <Provider store={store}>
      <Routes>
        <Route path="*" element={<NotFoundPage />} />
        <Route path="/" element={<Layout />}>
          <Route index element={<HomePage />} />
          <Route path="news:id" element={<StoryPage />} />
        </Route>
      </Routes>
    </Provider>
  );
}

export default App;
