import styles from "../styles/CustomButton.module.scss";

export const CustomButton = ({ disabled, handleClick, text, name }) => {
  return (
    <button
      className={styles.button}
      disabled={disabled}
      onClick={handleClick}
      name={name}
    >
      {text}
    </button>
  );
};
