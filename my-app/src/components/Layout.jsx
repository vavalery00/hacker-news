import { Outlet } from "react-router-dom";
import styles from "../styles/Layout.module.scss";
import { useNavigate } from "react-router-dom";
import logo from '../images/logo.svg';

export const Layout = () => {
  const navigate = useNavigate();
  return (
    <>
      <header className={styles.header}>
        <button
          className={styles.logo}
          onClick={() => navigate("/")}
        >
          <img src={logo}/>
        </button>
      </header>
      <main className={styles.main}>
        <Outlet />
      </main>
    </>
  );
};
