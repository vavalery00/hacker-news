import React from "react";
import styles from "../styles/Story.module.scss";

import ReactTimeAgo from "react-time-ago";

import heartImg from "../images/heart.svg";

import { useNavigate } from "react-router-dom";

export const Story = ({
  story: { id, by, title, kids, time, url, score },
  story,
}) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`news${id}`, { state: { kids } });
  };

  return (
    <div className={styles.story} onClick={handleClick}>
      <div className={styles.story__header}>
        <ReactTimeAgo date={time * 1000} locale="en-US" />
      </div>
      <div className={styles.story__body}>
        <span className={styles.story__title}>{title}</span>
      </div>
      <div className={styles.story__footer}>
        <span className={styles.story__author}>{by}</span>
        <div className={styles.scoreContainer}>
          <img height="30px" alt="" src={heartImg} />
          <span className={styles.story__score}>{score}</span>
        </div>
      </div>
    </div>
  );
};
