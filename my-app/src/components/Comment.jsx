import { useState, useEffect } from "react";
import styles from "../styles/Comment.module.scss";
import ReactTimeAgo from "react-time-ago";
import { getComments } from "../store/utils/apis";

export const Comment = ({ comment: { by, text, kids, time } }) => {
  const [replies, setReplies] = useState([]);
  const [isShown, setShown] = useState(false);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = () => {
      setLoading(true);
      getComments(kids)
        .then((replies) => setReplies(replies))
        .finally(() => setLoading(false));
    };

    fetchData();
  }, []);

  const handleToggle = () => {
    setShown((prev) => !prev);
  };

  return (
    <div className={styles.comment}>
      <div className={styles.comment__header}>
        <span className={styles.author}>{by}</span>
        <ReactTimeAgo
          className={styles.time}
          date={time * 1000}
          locale="en-US"
        />
      </div>
      <div className={styles.comment__body}>
        <span dangerouslySetInnerHTML={{ __html: text }} />
        {kids ? (
          <button
            className={styles.button}
            onClick={handleToggle}
            disabled={isLoading}
          >
            {isShown ? "Hide" : "Show"} replies
          </button>
        ) : null}
      </div>
      {isShown && (
        <div className={styles.replies}>
          {replies.map((comment) => (
            <Comment key={comment.id} comment={comment} />
          ))}
        </div>
      )}
    </div>
  );
};
