import preloaderImg from "../images/preloader.svg";
import styles from "../styles/Preloader.module.scss";
import cn from 'classnames';

export const Preloader = ({ loadingItem, className }) => {
  return (
    <div
      className={cn(styles.preloader, {
        [className]: !!className,
      })}
      name={loadingItem}
    >
      <img height="60px" alt="" src={preloaderImg} />
    </div>
  );
};
