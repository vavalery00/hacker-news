import React from "react";
import { Story } from "./Story";
import styles from "../styles/ShowStories.module.scss";

const ShowStories = ({ stories }) => {
  return (
    <div className={styles.storiesContainer}>
      {stories.map((story) => (
        <Story key={story.id} story={story} />
      ))}
    </div>
  );
};

export default ShowStories;
