import { useState, useEffect } from "react";
import ShowStories from "../components/ShowStories";
import styles from "../styles/HomePage.module.scss";
import ReactTimeAgo from "react-time-ago";
import { CustomButton } from "../components/CustomButton";

import { useDispatch, useSelector } from "react-redux";

import { fetchStories } from "../store/slices/newsListSlice";

import { useRef } from "react";
import { Preloader } from "../components/Preloader";

export const HomePage = () => {
  const [forceUpdate, setForceUpdate] = useState(false);
  const [updationTime, setUpdationTime] = useState(new Date());
  const intervalRef = useRef(null);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchStories());
    if (forceUpdate) {
      setForceUpdate(false);
      clearInterval(intervalRef.current);
      setUpdationTime(new Date());
    }
  }, [dispatch, forceUpdate]);

  useEffect(() => {
    intervalRef.current = setInterval(() => {
      dispatch(fetchStories());
      setUpdationTime(new Date());
    }, 600000);
    return () => clearInterval(intervalRef.current);
  }, [dispatch]);

  const handleClick = () => {
    setForceUpdate(true);
    clearInterval(intervalRef.current);
  };

  const { news: storiesList, status } = useSelector((state) => state.news);

  return (
    <div className={styles.homePage}>
      <div className={styles.buttonContainer}>
        <span className={styles.lastUpdation}>
          Last updation: <ReactTimeAgo date={updationTime} locale="en-US" />
        </span>
        <CustomButton
          name="update"
          disabled={status === "loading"}
          handleClick={handleClick}
          text="update"
        />
      </div>
      {status === "loading" ? (
        <Preloader
          className={styles.preloader}
        />
      ) : (
        <ShowStories stories={storiesList} />
      )}
    </div>
  );
};
