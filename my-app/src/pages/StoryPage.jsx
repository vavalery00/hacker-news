import { useEffect, useState } from "react";
import { Comment } from "../components/Comment";
import { useParams, useNavigate } from "react-router-dom";
import { CustomButton } from "../components/CustomButton";

import styles from "../styles/StoryPage.module.scss";
import { fetchStory, fetchComments } from "../store/slices/storySlice";

import { useDispatch, useSelector } from "react-redux";

import { Preloader } from "../components/Preloader";

import ReactTimeAgo from "react-time-ago";

import heartImg from "../images/heart.svg";
import commentImg from "../images/comment.svg";

const Link = ({ url, title }) => (
  <a className={styles.link} href={url} target="_blank" rel="noreferrer">
    {title}
  </a>
);

export const StoryPage = () => {
  const [forceUpdate, setForceUpdate] = useState(false);
  const [updationTime, setUpdationTime] = useState(new Date());
  const { id } = useParams();

  const navigate = useNavigate();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchStory(id));
  }, []);

  useEffect(() => {
    if (forceUpdate) {
      dispatch(fetchComments(id));
      setUpdationTime(new Date());
      setForceUpdate(false);
    }
  }, [forceUpdate]);

  const handleClick = () => {
    setForceUpdate(true);
  };

  const {
    story,
    commentsTotalCount,
    storyLoadingStatus,
    commentsLoadingStatus,
    rootComments,
  } = useSelector((state) => state.story);
  const date = story.time
    ? new Intl.DateTimeFormat("en-UK", {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
      }).format(story.time * 1000)
    : "";

  return (
    <>
      {storyLoadingStatus === "loading" ? (
        <Preloader
          className={styles.pagePreloader}
        />
      ) : (
        <div className={styles.story}>
          <div className={styles.storyCard}>
            <h1 className={styles.title}>{story.title}</h1>
            <span className={styles.info}>
              Posted by {story.by} on {date}
            </span>
            <div className={styles.storyFooter}>
              <div className={styles.scoreContainer}>
                <div className={styles.score}>
                  <img height="36px" alt="" src={heartImg} />
                  {story.descendants ? story.descendants : 0}
                </div>
                <div className={styles.score}>
                  <img height="30px" alt="" src={commentImg} />
                  {commentsTotalCount}
                </div>
              </div>
              <Link title="Read in source" url={story.url} />
            </div>
          </div>
          {rootComments.length ? (
            <div>
              <div className={styles.buttonContainer}>
                <div className={styles.oneMoreContainer}>
                  <h2 className={styles.sectionTitle}>Comments</h2>
                  <span className={styles.lastUpdation}>
                    last updation:{" "}
                    <ReactTimeAgo date={updationTime} locale="en-US" />
                  </span>
                </div>
                <CustomButton
                  name="update"
                  disabled={commentsLoadingStatus === "loading"}
                  handleClick={handleClick}
                  text="update"
                />
              </div>
              {commentsLoadingStatus === "loading" ? (
                <Preloader
                  className={styles.commentsPreloader}
                />
              ) : (
                <div className={styles.commentsContainer}>
                  {rootComments.map((el) => (
                    <div className={styles.threadContainer} key={el.id}>
                      <Comment comment={el} />
                    </div>
                  ))}
                </div>
              )}
            </div>
          ) : null}
        </div>
      )}
      <footer className={styles.footer}>
        <CustomButton
          name="back"
          className={styles.button}
          handleClick={() => {
            navigate(-1);
          }}
          text="Go back"
        />
      </footer>
    </>
  );
};
